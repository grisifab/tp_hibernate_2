package org.eclipse.model;

import javax.persistence.*;

@Entity
@NamedQueries({ 
	@NamedQuery( name="findByNomPrenom", 
			query="SELECT p FROM Personne p WHERE p.nom = :nom and p. prenom = :prenom" ), 
	@NamedQuery( name="findByPrenom", 
	query="SELECT p FROM Personne p WHERE p.prenom = :prenom" ), 
	}) 
public class Personne {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String prenom;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	public Personne(int id, String nom, String prenom) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
	}
	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
}
