package org.eclipse.main;

import java.util.List;


import org.eclipse.model.Personne;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Personne leGars = new Personne();
       leGars.setNom("de Mikonos");
       leGars.setPrenom("Nikos");
       
       System.out.println("jusqu'ici tout va bien 1");
       Configuration configuration = new Configuration().configure();
       System.out.println("jusqu'ici tout va bien 2");
       SessionFactory sessionFactory = configuration.buildSessionFactory();
       System.out.println("jusqu'ici tout va bien 3");
       Session session = sessionFactory.openSession();
       System.out.println("jusqu'ici tout va bien 4");
       Transaction transaction = session.beginTransaction();
       
      // Méthode 1
       System.out.println("jusqu'ici tout va bien 5");
       String sqlRequete = "select * from Personne"; 
       SQLQuery query = session.createSQLQuery(sqlRequete); 
       query.addEntity(Personne.class);
       List<Personne> personnes = (List<Personne>) query.list(); 
       for(Personne personne : personnes) System.out.println(personne);
       
    // Méthode 2
       System.out.println("jusqu'ici tout va bien 6");
       String sqlRequete2 = "select * from Personne where nom = :nom"; 
       SQLQuery query2 = session.createSQLQuery(sqlRequete2); 
       query2.addEntity(Personne.class); 
       query2.setParameter("nom", "Benguigui");
       List<Personne> personna = (List<Personne>) query2.list(); 
       for(Personne personne : personna) System.out.println(personne);
       
    // Méthode 3
       System.out.println("jusqu'ici tout va bien 7");
       Query query3 = session.getNamedQuery("findByNomPrenom");  // défini dans class Personne
       query3.setParameter("nom", "Wick"); 
       query3.setParameter("prenom", "John");
       List<Personne> personnaz = (List<Personne>) query3.list(); 
       for(Personne personne : personnaz) System.out.println(personne);
       
       System.out.println("jusqu'ici tout va bien 8");
       Query query4 = session.getNamedQuery("findByPrenom");  // défini dans class Personne
       query4.setParameter("prenom", "John"); 
       List<Personne> personnza = (List<Personne>) query4.list(); 
       for(Personne personne : personnza) System.out.println(personne);
       
       
       
       transaction.commit(); 
       session.close(); 
       sessionFactory.close();
       System.out.println("jusqu'ici tout va bien 9");
       
       
    }
}
